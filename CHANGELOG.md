# 1.28.0

## Release Notes
https://gitlab.com/asacolips-projects/foundry-mods/archmage/-/releases/1.28.0

![Foundry v11.315](https://img.shields.io/badge/Foundry-v11.315-green) ![Foundry v12](https://img.shields.io/badge/Foundry-v12-yellow)

## Summary

### Changes

- Added cyclic spells support
- Added Compendium Browser

### Fixes

- Fixed typos and format errors in a few monsters
- Improved rendering performance of character sheets with a large number of powers